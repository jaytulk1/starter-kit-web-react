import React, {Component, Fragment} from 'react';
import {Route, Switch} from 'react-router-dom';

import './MainLayout.css';
import NavBar from './NavBar';
import Page from './Page';
import UsersIndexScreen from '../users/UsersIndexScreen';
import {ROUTES} from '../../constants';

class MainLayout extends Component {
  render() {
    return (
      <Fragment>
        <NavBar
          currentUser={this.props.currentUser}
          handleLogout={this.props.handleLogout}
        />
        <div className="main-content-container">
          <Switch>
            <Route
              component={() => <Page header="Dashboard" />}
              exact
              path={ROUTES.dashboard}
            />
            <Route component={() => <UsersIndexScreen />} path={ROUTES.users} />
          </Switch>
        </div>
      </Fragment>
    );
  }
}

export default MainLayout;
