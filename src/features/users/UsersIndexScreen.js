import React, {Component} from 'react';
import Page from '../layout/Page';

class UsersIndexScreen extends Component {
  render() {
    return (
      <Page header="Users">
        <div>Manage some users here</div>
      </Page>
    );
  }
}

export default UsersIndexScreen;
